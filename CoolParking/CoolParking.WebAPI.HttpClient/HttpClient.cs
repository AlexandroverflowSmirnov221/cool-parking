using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.HttpClient
{

    class HttpClient
    {
        static readonly System.Net.Http.HttpClient client = new();
        static async Task Main()
        {
            bool close = false;
            string id;
            int vehicleType;
            int balance;
            int sum;
            VehicleFormation vf;
            while (!close)
            {
                char cmd = Display();
                Console.ReadKey();
                Console.Clear();
                switch (cmd)
                {
                    case '0': 
                        Console.WriteLine("Get Capacity : " + await GetCapacity());
                        break;


                    case '1': 
                        Console.WriteLine("Get Balance : " + await GetBalance());
                        break;


                    case '2': 
                        Console.WriteLine("Get Free places : " + await GetFreePlace());
                        break;


                    case '3': 
                        Console.WriteLine(" Get Last transactions : \n" + await GetLastTransactions());
                        break;


                    case '4': 
                        Console.WriteLine("Get All transactions : \n" + await GetAllTransactions());
                        break;


                    case '5':
                        List<VehicleFormation> list = await GetVehicles();
                        list.ForEach(vf =>
                        {
                            ShowVehicle(vf);
                            Console.WriteLine("/n");
                        });
                        break;


                    case '6': 
                        Console.WriteLine("Enter vehicle number");
                        id = Console.ReadLine();
                        ShowVehicle(await GetVehicle(id));
                        break;


                    case '7': 
                        Console.WriteLine("Enter vehicle number");
                        id = Console.ReadLine();
                        Console.WriteLine("Choose type of your vehicle");
                        Console.WriteLine("1 - PassengerCar, 2 - Truck, 3 - Bus, 4 - Motorcycle");
                        vehicleType = Convert.ToInt32(Console.ReadLine()) - 1;
                        Console.Write("Enter balance - ");
                        balance = Convert.ToInt32(Console.ReadLine());
                        vf = new VehicleFormation { Id = id, VehicleType = vehicleType, Balance = balance };
                        Console.WriteLine(await PostVehicles(vf));
                        break;


                    case '8':
                        Console.WriteLine("Enter vehicle number");
                        id = Console.ReadLine();
                        Console.WriteLine(await DeleteVehicle(id));
                        break;


                    case '9': 
                        Console.WriteLine("Enter vehicle number");
                        id = Console.ReadLine();
                        Console.WriteLine("Enter the top-up sum");
                        sum = Convert.ToInt32(Console.ReadLine());
                        TransactionsTopUpVehicleFormation tr = new() { Id = id, Sum = sum };
                        var vehicle = await PutTopUpVehicle(tr);
                        if (vehicle == null)
                        {
                            Console.WriteLine("Invalid data");
                        }
                        else
                        {
                            ShowVehicle(vehicle);
                        }
                        break;


                    case 'q': 
                        close = true;
                        break;


                    default:
                        Console.WriteLine("Invalid command ");
                        break;
                }
                Console.WriteLine("Press any key to continue");
                Console.ReadKey();
            }
        }

        static void ShowVehicle(VehicleFormation vf)
        {
            Console.WriteLine("Vehicle id: " + vf.Id);
            Console.WriteLine("Vehicle type: " + vf.VehicleType);
            Console.WriteLine("Vehicle balance: " + vf.Balance);
        }

        static char Display()
        {
            char Selected = ' ';
            Console.WriteLine("0. Current parking capacity");
            Console.WriteLine("1. Get Balance for the current period.");
            Console.WriteLine("2. Get free parking places.");
            Console.WriteLine("3. Get lLast Parking Transactions for the current period.");
            Console.WriteLine("4. Get all transactions.");
            Console.WriteLine("5. List of vehicles in the CoolParking.");
            Console.WriteLine("6. Get vehicle by id");
            Console.WriteLine("7. Add vehicles to CoolParking");
            Console.WriteLine("8. Delete vehicle from CoolParking.");
            Console.WriteLine("9. Top up the vehicle balance");
            Console.WriteLine("Exit - q");
            Selected = Console.ReadKey().KeyChar;

            return Selected;
        }

        static async Task<string> GetBalance()
        {
            return await GetRequest("https://localhost:44398/api/parking/balance");
        }

        static async Task<string> GetCapacity()
        {
            return await GetRequest("https://localhost:44398/api/parking/capacity");
        }

        static async Task<string> GetFreePlace()
        {
            return await GetRequest("https://localhost:44398/api/parking/freePlaces");
        }

        static async Task<string> GetRequest(string url)
        {
            HttpResponseMessage response = await client.GetAsync(url);
            if (response.StatusCode == HttpStatusCode.NotFound || response.StatusCode == HttpStatusCode.BadRequest)
                throw new ArgumentException("Invalid url or not find url");
            string request = await response.Content.ReadAsStringAsync();
            return request;
        }

        static async Task<HttpStatusCode> PostVehicles(VehicleFormation vf)
        {
            var url = "https://localhost:44398/api/vehicles";
            HttpContent request = new StringContent(JsonConvert.SerializeObject(vf), Encoding.UTF8, "application/json");
            var result = await client.PostAsync(url, request);
            return result.StatusCode;
        }

        static async Task<VehicleFormation> GetVehicle(string id)
        {
            var url = "https://localhost:44398/api/vehicles/" + id;
            
            HttpResponseMessage response = await client.GetAsync(url);
            if (response.StatusCode == HttpStatusCode.NotFound || response.StatusCode == HttpStatusCode.BadRequest)
                throw new ArgumentException("Invalid Id or not found Id");
            var vehicle = JsonConvert.DeserializeObject<VehicleFormation>(await response.Content.ReadAsStringAsync());
            return vehicle;
        }

        static async Task<List<VehicleFormation>> GetVehicles()
        {
            var url = "https://localhost:44398/api/vehicles";
            HttpResponseMessage response = await client.GetAsync(url);
            if (response.StatusCode == HttpStatusCode.NotFound || response.StatusCode == HttpStatusCode.BadRequest)
                throw new ArgumentException("Invalid response");
            var vehicle = JsonConvert.DeserializeObject<List<VehicleFormation>>(await response.Content.ReadAsStringAsync());
            return vehicle;
        }

        static async Task<HttpStatusCode> DeleteVehicle(string id)
        {
            var url = "https://localhost:44398/api/vehicles/" + id;
            HttpResponseMessage response = await client.DeleteAsync(url);
            if (response.StatusCode == HttpStatusCode.NotFound || response.StatusCode == HttpStatusCode.BadRequest)
                throw new ArgumentException("Invalid Id or not found Id");
            return response.StatusCode;
        }

        static async Task<string> GetLastTransactions()
        {
            HttpResponseMessage response = await client.GetAsync("https://localhost:44398/api/transactions/last");
            if (response.StatusCode == HttpStatusCode.NotFound)
                throw new ArgumentException("File not found");
            string lastTransactions = await response.Content.ReadAsStringAsync();
            return lastTransactions;
        }

        static async Task<string> GetAllTransactions()
        {
            HttpResponseMessage response = await client.GetAsync("https://localhost:44398/api/transactions/all");
            if (response.StatusCode == HttpStatusCode.NotFound)
                throw new ArgumentException("File not found");
            string allTransactions = await response.Content.ReadAsStringAsync();
            return allTransactions;
        }

        static async Task<VehicleFormation> PutTopUpVehicle(TransactionsTopUpVehicleFormation tr)
        {
            var url = "https://localhost:44398/api/transactions/topUpVehicle";
            HttpContent request = new StringContent(JsonConvert.SerializeObject(tr), Encoding.UTF8, "application/json");
            var result = await client.PutAsync(url, request);
            if (result.StatusCode == HttpStatusCode.OK)
            {
                var vehicle = JsonConvert.DeserializeObject<VehicleFormation>(await result.Content.ReadAsStringAsync());
                return vehicle;
            }
            else
            {
                return null;
            }

        }
    }
    public class TransactionsTopUpVehicleFormation
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("sum")]
        public decimal Sum { get; set; }
    }

    public class VehicleFormation
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("vehicleType")]
        public int VehicleType { get; set; }

        [JsonProperty("balance")]
        public decimal Balance { get; set; }

    }
}
using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking instance;
        public decimal CurentBalance { get; set; } = Settings.InitialParkingBalance;
        public decimal Balance { get; set; } = Settings.InitialParkingBalance;
        public IList<TransactionInfo> TransactionInfos { get; set; }
        public IList<Vehicle> Vehicles { get; set; }
        public Dictionary<int, bool> PlaceToPark { get; set; }

        private Parking()
        {
            Trigger();
        }

        private void Trigger()
        {
            PlaceToPark = new Dictionary<int, bool>();
            for (int i = 0; i < Settings.ParkingCapacity; i++)
                PlaceToPark.Add(i, true);
            Vehicles = new List<Vehicle>();
            TransactionInfos = new List<TransactionInfo>();
        }

        public static Parking ParkingIsSingleton()
        {
            if (instance == null)
                instance = new Parking();
            return instance;
        }

        public int RecieverForFreeParkingPlace()
        {
            for (int i = 0; i < PlaceToPark.Count; i++)
            {
                if (PlaceToPark[i])
                    return i;
            }
            throw new InvalidOperationException();
        }
    }
}
using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public decimal Sum { get; set; }
        public string VehicleId { get; set; }
        public DateTime TransactionTime { get; set; }

        public TransactionInfo(decimal sum, string vehicleId, DateTime transactionTime)
        {
            Sum = sum;
            VehicleId = vehicleId;
            TransactionTime = transactionTime;
        }   
    }
}
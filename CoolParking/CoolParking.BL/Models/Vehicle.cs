using System; 
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
  public class  Vehicle
    {
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            Regex regex = new(@"[A-Z]{2}-\d{4}-[A-Z]{2}");
            if (!(regex.IsMatch(id)) || balance < 0)
                throw new ArgumentException();
            else
            { 
                Id = id;
                VehicleType = vehicleType;
                Balance = balance;
            }
        }
        public string Id { get; private set; }
        public VehicleType VehicleType { get; private set; }
        public decimal Balance { get; set; }
        public int NumberForVehicle { get; set; }
        
        public Vehicle(VehicleType vehicleType, decimal balance)
        {
            Id = GenerateRandomRegistrationPlateNumber();
            VehicleType = vehicleType;
            Balance = balance;
        }

        public void TopUp(decimal sum)
        {
            if (sum <= 0)
                Balance += sum;
            else
                throw new ArgumentException();
        }

        public void Withdraw(decimal sum)
        {
            if (sum > 0) 
                Balance -= sum;
            else 
                throw new ArgumentException();
        }
        public static Vehicle FindVehicleById(string Id)
        {
            Parking parking = Parking.ParkingIsSingleton();
            for (int i = 0; i < parking.Vehicles.Count; i++)
            {
                if (parking.Vehicles[i].Id == Id)
                    return parking.Vehicles[i];
            }
            return null;
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            var number = GenerateRandomNumber();
            var firstLetter = GenerateRandomLetter();
            var lastLetter = GenerateRandomLetter();

            return String.Format("{0}-{1}-{2}", firstLetter, number, lastLetter);
        }

        public static string GenerateRandomLetter()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var randomCharacter = new Random();
            var stringChars = new char[2];

            for (int i = 0; i < 2; i++)
            {
                stringChars[i] = chars[randomCharacter.Next(chars.Length)];
            }

            string result = new (stringChars);
            return result;
        }

        private static string GenerateRandomNumber()
        {
            Random r = new ();
            int rInt = r.Next(1000, 9999);
            return rInt.ToString();
        } 
  }            
}
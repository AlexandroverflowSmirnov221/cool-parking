using System.Collections.Generic;

namespace CoolParking.BL.Models
{
   public static class Settings
    {
      public static int InitialParkingBalance = 0;
      public static int ParkingCapacity = 10;
      public static int PaymentPeriod = 5000;
      public static int LoggingPeriod = 12000;
      public static decimal PenaltyRate = 2.5M; 
      public static string LogFilePath { get; set; } = @"C:\Log\Transactions.log";

      public static Dictionary<VehicleType, decimal> CoolParkingTariff = new ()
        {
            {VehicleType.PassengerCar, 2M},
            {VehicleType.Truck, 5M},
            {VehicleType.Bus, 3.5M},
            {VehicleType.Motorcycle, 1M}
        };
    }
}
using System;
using System.Collections.ObjectModel;
using System.IO;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Linq;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {   
        public ITimerService WithdrawTimer { get; set; }
        public ITimerService LogTimer { get; set; }
        public ILogService LogFileService { get; set; }
        private Parking CustomerService { get; set; } = Parking.ParkingIsSingleton();
        private bool IdIsNotEqualTo(string vehicleId) => !CustomerService.Vehicles.Any(Vehicle => Vehicle.Id == vehicleId);

        public ParkingService(bool TimerStart)
        {
            if (TimerStart)
            {
                WithdrawTimer = new TimerService();
                WithdrawTimer.Start();
            }
            LogFileService = new LogService(Settings.LogFilePath);
        }

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            WithdrawTimer = withdrawTimer;
            LogTimer = logTimer;
            LogFileService = logService;
        }
        
        public Vehicle GetVehicle(string vehicleId)
        {
            try
            {
                if (!IdIsNotEqualTo(vehicleId))
                {
                    throw new InvalidDataException();
                }
                Vehicle vehicle =  CustomerService.Vehicles.Single(v => v.Id == vehicleId);

                return vehicle;
            }
            catch (InvalidOperationException)
            {
                throw new ArgumentException();
            }
        }

        public decimal GetBalance()
        {
            if (CustomerService.Balance <= Settings.InitialParkingBalance)
                throw new InvalidOperationException();
            return CustomerService.Balance;
        }

        public int GetCapacity()
        {
            return Settings.ParkingCapacity;
        }

        public int GetFreePlaces()
        {
            return GetCapacity() - CustomerService.Vehicles.Count;
        }
        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            ReadOnlyCollection<Vehicle> collection = new (CustomerService.Vehicles);
            return collection;
        }

        public void AddVehicle(Vehicle vehicle)
        {

            if (CustomerService.Vehicles.Count > Settings.ParkingCapacity)
                throw new InvalidOperationException("Parking Capacity is full");
            if (IdIsNotEqualTo(vehicle.Id))
            {
                CustomerService.Vehicles.Add(vehicle);
                CustomerService.PlaceToPark[CustomerService.RecieverForFreeParkingPlace()] = false;
                vehicle.NumberForVehicle = CustomerService.RecieverForFreeParkingPlace();
                return;
            }
            throw new ArgumentException();

        }

        public void RemoveVehicle(string vehicleId)
        {
            if (!IdIsNotEqualTo(vehicleId))
            {
                if (Vehicle.FindVehicleById(vehicleId).Balance < 0)
                    throw new InvalidOperationException("Cannot remove vehicle with negative balance");
                CustomerService.PlaceToPark[Vehicle.FindVehicleById(vehicleId).NumberForVehicle] = true;
                CustomerService.Vehicles.Remove(Vehicle.FindVehicleById(vehicleId));
                return;
            }
            throw new ArgumentException("Id doesn't exist");
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum <= 0)
                throw new ArgumentException("Negative value");
            Vehicle vehicle = Vehicle.FindVehicleById(vehicleId);
            if (vehicle != null)
            {
                vehicle.Balance += sum;
                Console.WriteLine("Balance for this vehicle " + vehicle.Balance);
                return;
            }
            throw new ArgumentException("Id doesn't exist");
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return CustomerService.TransactionInfos.ToArray();
        }

        public string ReadFromLog()
        {
            LogService log = new (Settings.LogFilePath);
            return log.Read();
        }

        public void Dispose()
        {
            CustomerService.Balance = Settings.InitialParkingBalance;
            CustomerService.PlaceToPark.Clear();
            for (int i = 0; i < Settings.ParkingCapacity; i++)
                CustomerService.PlaceToPark.Add(i, true);
            CustomerService.Vehicles.Clear();
            WithdrawTimer.Dispose();
            if (File.Exists(Settings.LogFilePath))
                File.Delete(Settings.LogFilePath);
            GC.Collect();
        }
    }
}
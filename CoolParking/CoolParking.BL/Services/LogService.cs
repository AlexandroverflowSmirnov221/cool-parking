using System;
using System.IO;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        private string logPath;
        public string LogPath
        {
            get => logPath;

            private set
            {
                if (value != null)
                {
                    logPath = value;
                }
                else
                {
                    throw new ArgumentNullException();
                }
            }
        }
        public void Dispose()
        {
            File.Delete(logPath);
        }

        public LogService(string logPath)
        {
            LogPath = logPath;
        }

        public string Read()
        {
            string readedLogInfo = null;
            try
            {
                using StreamReader streamReader = new (logPath);
                if (streamReader == null)
                    throw new InvalidOperationException();
                readedLogInfo = streamReader.ReadToEnd();
                streamReader.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot read the file");
                Console.WriteLine(e.Message);
            }
            return readedLogInfo;
        }

        public void Write(string logInfo)
        {
            try
            {
                using StreamWriter streamWriter = new (logPath, true);
                streamWriter.WriteLine(logInfo);
                streamWriter.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Cannot write the file");
                Console.WriteLine(ex.Message);
            }
        }
    }
}
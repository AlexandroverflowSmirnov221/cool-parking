using System;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Threading.Tasks;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private readonly Parking parking = Parking.ParkingIsSingleton();
        ILogService LogService { get; set; }
        public double Interval { get; set; }
        private Timer PaymentTimer { get; set; }
        public Timer LoggingTimer { get; set; }

        public event ElapsedEventHandler Elapsed;
        public void FireElapsedEvent()
        {
            Elapsed?.Invoke(this, null);
        }

        public TimerService()
        {

            Elapsed += (sender, e) => RegularWithdraw(sender, e);
            LogService = new LogService(Settings.LogFilePath);
        }
        public void Dispose()
        {
            if (PaymentTimer != null)
                PaymentTimer.Dispose();
            if (LoggingTimer != null)
                LoggingTimer.Dispose();
        }

        public void Start()
        {
            Interval = Settings.PaymentPeriod;
            PaymentTimer = new Timer(Interval);
            PaymentTimer.Elapsed += async (sender, e) => await Task.Run(() => RegularWithdraw(sender, e));
            PaymentTimer.Start();


            Interval = Settings.LoggingPeriod;
            LoggingTimer = new Timer(Interval);
            LoggingTimer.Elapsed += async (sender, e) => await Task.Run(() => LogTransactions(sender, e));
            LoggingTimer.Start();

        }

        public void Stop()
        {
            if (PaymentTimer != null)
            {
                PaymentTimer.Stop();
            }
            if (LoggingTimer != null)
                LoggingTimer.Stop();
        }

        private void LogTransactions(object sender, ElapsedEventArgs e)
        {
            var transaction = parking.TransactionInfos;
            for (int i = 0; i < transaction.Count; i++)
            {
                LogTransaction(transaction[i]);
            }
            parking.TransactionInfos.Clear();
            parking.CurentBalance = 0;

        }
        private void LogTransaction(TransactionInfo transaction) => LogService.Write($"{transaction.TransactionTime:T}\n{transaction.VehicleId}\n{transaction.Sum}\n\n");

        private void RegularWithdraw(object sender, ElapsedEventArgs e)
        {
            foreach (Vehicle v in parking.Vehicles)
            {
                decimal withdrawnSum = CalculateWithdrawnSum(v);
                v.Withdraw(withdrawnSum);
                parking.TransactionInfos.Add(CompileTransactionInfo(v, withdrawnSum));
                Console.WriteLine("{1} was withdrawn from the vehicle with id {0}. Vehicle balance is {2}", v.Id, withdrawnSum, v.Balance);
            }
            Console.WriteLine("\n");
            TransactionInfo CompileTransactionInfo(Vehicle vehicle, decimal withdrawnSum)
            {
                TransactionInfo transactionInfo = new (vehicle.Balance, vehicle.Id, DateTime.Now);
                return transactionInfo;
            }

            decimal CalculateWithdrawnSum(Vehicle vehicle)
            {
                decimal withdrawnSum;
                Settings.CoolParkingTariff.TryGetValue(vehicle.VehicleType, out withdrawnSum);
                if (vehicle.Balance >= withdrawnSum)
                {
                    return withdrawnSum;
                }
                else if (vehicle.Balance == 0)
                {
                    return withdrawnSum * Settings.PenaltyRate;
                }

                else if (vehicle.Balance < withdrawnSum && vehicle.Balance > 0)
                {
                    return ((withdrawnSum - vehicle.Balance) * Settings.PenaltyRate + (vehicle.Balance));
                }
                else if (vehicle.Balance < withdrawnSum)
                {
                    return withdrawnSum * Settings.PenaltyRate;
                }
                else
                {
                    return 0;
                }
            }
        }
    }
}
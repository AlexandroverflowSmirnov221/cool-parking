using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        // GET: api/balance
        [HttpGet("balance")]
        public ActionResult<decimal> GetBalance() => Ok(Startup.parkingService.GetBalance());
       

        // GET: api/capacity
        [HttpGet("capacity")]
        public ActionResult<int> GetCapacity() => Ok(Startup.parkingService.GetCapacity());
       

        // GET: api/freePlaces
        [HttpGet("freePlaces")]
        public ActionResult<int> GetFreePlaces() => Ok(Startup.parkingService.GetFreePlaces());
        
    }
}
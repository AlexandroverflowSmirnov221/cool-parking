using System;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        // GET: api/Vehicles
        [HttpGet]
        public ActionResult<VehicleFormation> Get()
        {
            return Ok(Startup.parkingService.GetVehicles());
        }

        // POST: api/Vehicles
        [HttpPost]
        public ActionResult<VehicleFormation> Post([FromBody] VehicleFormation vehicleFormation)
        {

            if (vehicleFormation == null)
            {
                return BadRequest();
            }
            try
            {
                Vehicle vehicle = new (vehicleFormation.Id, (VehicleType)vehicleFormation.VehicleType, vehicleFormation.Balance);

                Startup.parkingService.AddVehicle(vehicle);
                return CreatedAtAction("Post", vehicleFormation);
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }

        //GET api/Vehicles/id
        [HttpGet("{id}")]
        public ActionResult GetId([FromRoute] string id)
        {
            try
            {
                Vehicle vehicle = Startup.parkingService.GetVehicle(id);
                VehicleFormation vb = new()
                {
                    Id = vehicle.Id,
                    VehicleType = (int)vehicle.VehicleType,
                    Balance = vehicle.Balance
                };
                return Ok(vb);
            }
            catch (ArgumentException)
            {
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // DELETE api/vehicles/id
        [HttpDelete("{id}")]
        public ActionResult Delete([FromRoute] string id)
        {

            try
            {
                Startup.parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch (ArgumentException)
            {
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }

        public class VehicleFormation
        {
            [JsonProperty("id")]
            public string Id { get; set; }

            [JsonProperty("vehicleType")]
            public int VehicleType { get; set; }

            [JsonProperty("balance")]
            public decimal Balance { get; set; }

        }
    }
}
using System;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Models;
using System.Net;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        // GET: api/Transactions/last
        [HttpGet("last")]
        public ActionResult GetLastTransaction()
        {
            Response.StatusCode = (int)HttpStatusCode.OK;
            TransactionInfo[] lastTransactions = Startup.parkingService.GetLastParkingTransactions();
            TransactionsFormation[] lastTransactionsFormation = new TransactionsFormation[lastTransactions.Length];
            int i = 0;
            foreach (TransactionInfo item in lastTransactions)
            {
                TransactionsFormation tf = new();
                tf.VehicleId = item.VehicleId;
                tf.Sum = item.Sum;
                tf.TransactionDate = item.TransactionTime;
                lastTransactionsFormation[i] = tf;
                i++;
            }
            return Ok(lastTransactionsFormation);
        }

        // GET: api/Transactions/all
        [HttpGet("all")]
        public ActionResult<string> GetAllTransaction()
        {
            try
            {
                return Ok(Startup.parkingService.ReadFromLog());
            }
            catch (Exception) 
            {
                return NotFound();
            }
        }

        // PUT: api/Transactions/topUpVehicle
        [HttpPut("topUpVehicle")]
        public ActionResult PutTopUpVehicle([FromBody] TransactionsTopUpVehicleFormation transactionsTopUpVehicleFormation)
        {
            if (transactionsTopUpVehicleFormation == null)
            {
                return BadRequest();
            }
            try
            {
                Vehicle vehicle = Startup.parkingService.GetVehicle(transactionsTopUpVehicleFormation.Id);
                Startup.parkingService.TopUpVehicle(transactionsTopUpVehicleFormation.Id, transactionsTopUpVehicleFormation.Sum);
                vehicle = Startup.parkingService.GetVehicle(transactionsTopUpVehicleFormation.Id);
                return Ok(JsonConvert.SerializeObject(vehicle));
            }
            catch (ArgumentException)
            {
                return NotFound();
            }
            catch (Exception) 
            {
                return BadRequest();
            }
        }

    }

    public class TransactionsFormation
    {
        [JsonProperty("vehicleId")]
        public string VehicleId { get; set; }
        [JsonProperty("sum")]
        public decimal Sum { get; set; }
        [JsonProperty("transactionDate")]
        public DateTime TransactionDate { get; set; }
    }
    
    public class TransactionsTopUpVehicleFormation
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("sum")]
        public decimal Sum { get; set; }
    }
}